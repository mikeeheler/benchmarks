#include <cassert>
#include <algorithm>
#include <chrono>
#include <fstream>
#include <functional>
#include <iostream>
#include <random>
#include <vector>

namespace ch = ::std::chrono;
using ::std::cout;
using ::std::vector;

template<class Iter>
void quicksort(Iter first, Iter last) {
  typename Iter::difference_type const size = last - first;
  
  if (size < 2)
    return;

  // Pick middle pivot to avoid worst-case situations
  if (size > 16)
    std::iter_swap(first, first + size/2);

  auto i = first + 1;
  auto j = i;

  while (j < last) {
    if (*j < *first)
      std::iter_swap(i++, j);
    ++j;
  }

  std::iter_swap(first, i-1);

  quicksort(first, i);
  quicksort(i, last);
}


int main(int argc, char * argv[]) {
  size_t const data_size = 16777216;

  std::random_device rd;
  std::mt19937 generator(rd());
  std::uniform_int_distribution<size_t> random_number(0, data_size);

  cout << "Populating data..." << std::flush;
  auto start = ch::high_resolution_clock::now();
  vector<size_t> data;
  data.reserve(data_size);
  for (size_t i = 0; i < data_size; ++i)
    data.emplace_back(random_number(generator));
  auto stop = ch::high_resolution_clock::now();
  cout << ch::duration_cast<ch::milliseconds>(stop - start).count() << "ms\n";

  /*std::ofstream init_data("C:/temp/init_data.txt");
  for (auto const & i : data)
    init_data << i << '\n';
  init_data.close();*/

  cout << "Sorting..." << std::flush;
  start = ch::high_resolution_clock::now();
  quicksort(begin(data), end(data));
  stop = ch::high_resolution_clock::now();
  cout << ch::duration_cast<ch::milliseconds>(stop - start).count() << "ms\n";

  /*std::ofstream sorted_data("C:/temp/sorted_data.txt");
  for (auto const & i : data)
    sorted_data << i << '\n';
  sorted_data.close();*/

  return 0;
}

