#include <cassert>
#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <random>
#include <vector>

namespace ch = ::std::chrono;
using ::std::cout;
using ::std::vector;

template<class Iter, class TempIter>
void mergesort(Iter first, Iter last, TempIter temp) {
  typename Iter::difference_type size = last - first;

  if (size < 2)
    return;

  if (size == 2) {
    if (*(last-1) < *first)
      std::iter_swap(first, last-1);
    return;
  }

  auto middle = first + size / 2;

  mergesort(first, middle, temp);
  mergesort(middle, last, temp);

  // Merge
  auto i = first;
  auto j = middle;
  auto sorted = temp;

  while (i < middle && j < last) {
    if (*i < *j)
      *sorted++ = *i++;
    else
      *sorted++ = *j++;
  }

  // Handle uneven lengths
  while (i < middle)
    *sorted++ = *i++;

  while (j < last)
    *sorted++ = *j++;

  std::copy(temp, temp + size, first);
}

template<class Iter>
void mergesort(Iter first, Iter last) {
  vector<typename Iter::value_type> temp(last - first);
  mergesort(first, last, begin(temp));
}

int main(int argc, char * argv[]) {
  std::random_device rd;
  std::mt19937 generator(rd());
  std::uniform_int_distribution<size_t> random_number(0, 16777216);

  cout << "Populating data..." << std::flush;
  auto start = ch::high_resolution_clock::now();
  vector<size_t> data;
  data.reserve(16777216);
  for (size_t i = 0; i < 16777216; ++i)
    data.emplace_back(random_number(generator));
  auto stop = ch::high_resolution_clock::now();
  cout << ch::duration_cast<ch::milliseconds>(stop - start).count() << "ms\n";

  /*std::ofstream init_data("C:/temp/init_data.txt");
  for (auto const & i : data)
    init_data << i << '\n';
  init_data.close();*/

  cout << "Sorting..." << std::flush;
  start = ch::high_resolution_clock::now();
  mergesort(begin(data), end(data));
  stop = ch::high_resolution_clock::now();
  cout << ch::duration_cast<ch::milliseconds>(stop - start).count() << "ms\n";

  /*std::ofstream sorted_data("C:/temp/sorted_data.txt");
  for (auto const & i : data)
    sorted_data << i << '\n';
  sorted_data.close();*/

  return 0;
}

