#include <cassert>
#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <random>
#include <vector>

namespace ch = ::std::chrono;
using ::std::cout;
using ::std::vector;

template<class Iter>
void siftdown(Iter first, Iter last, typename Iter::difference_type offset = 0) {
  typename Iter::difference_type size = last - first;

  if (size < 2)
    return;

  auto parent = first + offset;
  auto child = first + offset * 2 + 1;

  while (child < last) {
    if (child < last - 1 && *child < *(child+1))
      ++child;

    if (*parent < *child) {
      std::iter_swap(parent, child);
      offset = child - first;
      if (offset * 2 + 1 >= size)
        break;
      parent = first + offset;
      child = first + offset * 2 + 1;
    } else {
      break;
    }
  }
}

template<class Iter>
void heapify(Iter first, Iter last) {
  typename Iter::difference_type size = last - first;
  if (size < 2)
    return;

  auto offset = (size - 2) / 2;
  while (offset >= 0)
    siftdown(first, last, offset--);
}

template<class Iter>
void heapsort(Iter first, Iter last) {
  typename Iter::difference_type const size = last - first;
  if (size < 2)
    return;

  if (size == 2) {
    if (*first > *(last-1))
      std::iter_swap(first, last-1);
    return;
  }
  
  heapify(first, last);

  while (last > first) {
    std::iter_swap(first, --last);
    siftdown(first, last);
  }
}


int main(int argc, char * argv[]) {
  size_t const data_size = 16777216;

  std::random_device rd;
  std::mt19937 generator(rd());
  std::uniform_int_distribution<size_t> random_number(0, data_size);

  cout << "Populating data..." << std::flush;
  auto start = ch::high_resolution_clock::now();
  vector<size_t> data;
  data.reserve(data_size);
  for (size_t i = 0; i < data_size; ++i)
    data.emplace_back(random_number(generator));
  auto stop = ch::high_resolution_clock::now();
  cout << ch::duration_cast<ch::milliseconds>(stop - start).count() << "ms\n";

  std::ofstream init_data("C:/temp/init_data.txt");
  for (auto const & i : data)
    init_data << i << '\n';
  init_data.close();

  cout << "Sorting..." << std::flush;
  start = ch::high_resolution_clock::now();
  heapsort(begin(data), end(data));
  stop = ch::high_resolution_clock::now();
  cout << ch::duration_cast<ch::milliseconds>(stop - start).count() << "ms\n";

  std::ofstream sorted_data("C:/temp/sorted_data.txt");
  for (auto const & i : data)
    sorted_data << i << '\n';
  sorted_data.close();

  return 0;
}

