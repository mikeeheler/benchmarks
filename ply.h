#pragma once
#ifndef PLY_H_
#define PLY_H_ 1

#include <cstdint>
#include <istream>
#include <string>
#include <vector>

class PLYMesh {
 public:
  struct Vertex {
    float x;
    float y;
    float z;
    float confidence;
    float intensity;
  };

  struct Face {
    uint32_t v[3];
  };

  PLYMesh(std::string filename);
  ~PLYMesh() = default;

 private:
  enum eFormat {
    kUnknown,
    kASCII,
    kBinary
  };

  struct Element {
    std::string name;
    size_t count;
    std::vector<std::string> properties;
  };

  void ReadHeader(std::istream &);
  void ReadElement(std::istream &, Element &);
  void ReadVertices(std::istream &);
  void ReadFaces(std::istream &);

  bool is_ply_;
  eFormat format_;
  std::string format_version_;
  std::vector<Element> elements_;
  std::vector<Vertex> vertices_;
  std::vector<Face> faces_;
};

#endif // PLY_H_
