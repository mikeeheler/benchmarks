#include <chrono>
#include <fstream>
#include <iostream>
#include <random>
#include <string>
#include <thread>
#include <vector>
#include <glm/glm.hpp>
#include <Eigen/AlignedVector3>
#include <Eigen/Eigen>
#include <Eigen/StdVector>

#if defined(_MSC_VER)
# define ALIGN(x) __declspec(align(x))
#else
# define ALIGN(x) __attribute__((__aligned__(x)))
#endif // ALIGN

namespace ch = ::std::chrono;
using ::std::cout;
using ::std::vector;

size_t const g_vertex_count = 4194304;
size_t const g_joint_count = 4096;

std::random_device rd;
std::mt19937 g(rd());

std::uniform_real_distribution<float> point_rn(-1000.0f, 1000.0f);
std::uniform_real_distribution<float> vector_rn(-1.0f, 1.0f);
std::uniform_real_distribution<float> zeroone_rn(0.0f, 1.0f);

vector<float> g_points;
vector<float> g_normals;
vector<float> g_tangents;
vector<float> g_uvs;
vector<float> g_colours;

void Test_Eigen() {
  using namespace Eigen;
  
  using vec4 = Vector4f;
  using vec3 = Vector3f;
  using vec2 = Vector2f;
  using mat4 = Matrix4f;
  
  struct ALIGN(16) Joint {
    mat4 bind_inverse_m;
    mat4 pose_m;
  };
  
  struct ALIGN(16) Vertex {
    vec4 position;
    vec3 normal;
    vec3 tangent;
    vec2 uv;
    vec4 colour;
    vec4 weights;
    uint16_t bones[4];
  };
  
  struct ALIGN(16) Mesh {
    mat4 world_m;
    mat4 bind_m;
    vector<Vertex> vertices;
  };
  
  cout << "Eigen: Populating struct..." << std::flush;
  auto start = ch::high_resolution_clock::now();
  
  vector<Joint> joints;
  joints.reserve(g_joint_count);
  for (size_t i = 0; i < g_joint_count; ++i) {
    joints.emplace_back(Joint{
      Affine3f{Translation3f{point_rn(g), point_rn(g), point_rn(g)}}.matrix(),
      Affine3f{AngleAxisf{vector_rn(g), vec3::UnitZ()}}.matrix()});
  }
  
  Mesh mesh;
  mesh.world_m =
    Affine3f{Translation3f{point_rn(g), point_rn(g), point_rn(g)}}.matrix();
  mesh.bind_m =
    Affine3f{Translation3f{point_rn(g), point_rn(g), point_rn(g)} *
             AngleAxisf{vector_rn(g), vec3::UnitX()}}.matrix();
             
  std::uniform_int_distribution<size_t> joint_rn(0, g_joint_count-1);
  mesh.vertices.reserve(g_vertex_count);
  for (size_t i = 0; i < g_vertex_count; ++i) {
    mesh.vertices.emplace_back(Vertex{
      vec4{&g_points[i*4]},
      vec3{g_normals[i*3], g_normals[i*3+1], g_normals[i*3+2]},
      vec3{g_tangents[i*3], g_tangents[i*3+1], g_tangents[i*3+2]},
      vec2{&g_uvs[i*2]},
      vec4{&g_colours[i*4]},
      vec4{zeroone_rn(g), zeroone_rn(g), zeroone_rn(g),
           zeroone_rn(g)}.normalized(),
      {static_cast<uint16_t>(joint_rn(g)),
       static_cast<uint16_t>(joint_rn(g)),
       static_cast<uint16_t>(joint_rn(g)),
       static_cast<uint16_t>(joint_rn(g))}});
  }
  
  auto stop = ch::high_resolution_clock::now();
  cout << ch::duration_cast<ch::milliseconds>(stop - start).count() << "ms\n";
  
  cout << "Eigen: Deform mesh..." << std::flush;
  start = ch::high_resolution_clock::now();
  
  auto do_deform = [&](size_t start_index, size_t count) {
    for (size_t i = start_index; i < count; ++i) {
      auto & vertex = mesh.vertices[i];
      
      vec4 position{vec4::Zero()};
      vec3 normal{0.0f, 0.0f, 0.0f};
      vec3 tangent{0.0f, 0.0f, 0.0f};
      
      for (size_t i = 0; i < 4; ++i) {
        position += joints[vertex.bones[i]].pose_m *
                    joints[vertex.bones[i]].bind_inverse_m *
                    mesh.bind_m *
                    vertex.position;
        normal += joints[vertex.bones[i]].pose_m.topLeftCorner<3,3>() *
                  joints[vertex.bones[i]].bind_inverse_m.topLeftCorner<3,3>() *
                  mesh.bind_m.topLeftCorner<3,3>() *
                  vertex.normal;
        tangent += joints[vertex.bones[i]].pose_m.topLeftCorner<3,3>() *
                   joints[vertex.bones[i]].bind_inverse_m.topLeftCorner<3,3>() *
                   mesh.bind_m.topLeftCorner<3,3>() *
                   vertex.tangent;
      }
      
      vertex.position = mesh.world_m * position;
      vertex.normal = (mesh.world_m.topLeftCorner<3,3>() * normal).normalized();
      vertex.tangent = (mesh.world_m.topLeftCorner<3,3>() * tangent).normalized();
    }
  };
  
  using std::thread;

  size_t offset = 0;
  size_t thread_count = 32;
  size_t inc = g_vertex_count / thread_count;
  vector<thread> threads;
  for (size_t i = 0; i < thread_count; ++i) {
    threads.emplace_back(do_deform, offset, std::min(inc, g_vertex_count - offset));
    offset += inc;
  }
  for (auto & thread : threads) {
    if (thread.joinable())
      thread.join();
  }
  
  stop = ch::high_resolution_clock::now();
  cout << ch::duration_cast<ch::milliseconds>(stop - start).count() << "ms\n";

  struct PackedVertex {
    float position[3];
    float normal[3];
    float tangent[3];
    float uv[2];
    float colour[4];
  };

  vector<PackedVertex> packed_verts;
  packed_verts.reserve(mesh.vertices.size());
  for (auto const & vert : mesh.vertices) {
    packed_verts.emplace_back(PackedVertex{
        {vert.position[0], vert.position[1], vert.position[2]},
        {vert.normal[0], vert.normal[1], vert.normal[2]},
        {vert.tangent[0], vert.tangent[1], vert.tangent[2]},
        {vert.uv[0], vert.uv[1]},
        {vert.colour[0], vert.colour[1], vert.colour[2], vert.colour[3]}
    });
  }

  std::ofstream file("C:/temp/eigen.bin", std::ios::binary);
  file.write((char *)packed_verts.data(), packed_verts.size() * sizeof(PackedVertex));
  file.close();
}

void Test_GLM() {
  using namespace glm;
}

int main(int argc, char * argv[]) {
  cout << "Initialize data..." << std::flush;
  auto start = ch::high_resolution_clock::now();
  g_points.reserve(g_vertex_count*4);
  g_normals.reserve(g_vertex_count*3);
  g_tangents.reserve(g_vertex_count*3);
  g_uvs.reserve(g_vertex_count*2);
  g_colours.reserve(g_vertex_count*4);
  
  for (size_t i = 0; i < g_vertex_count; ++i) {
    g_points.emplace_back(point_rn(g));
    g_points.emplace_back(point_rn(g));
    g_points.emplace_back(point_rn(g));
    g_points.emplace_back(1.0f);
    
    g_normals.emplace_back(vector_rn(g));
    g_normals.emplace_back(vector_rn(g));
    g_normals.emplace_back(vector_rn(g));
    
    g_tangents.emplace_back(vector_rn(g));
    g_tangents.emplace_back(vector_rn(g));
    g_tangents.emplace_back(vector_rn(g));
    
    g_uvs.emplace_back(zeroone_rn(g));
    g_uvs.emplace_back(zeroone_rn(g));
    
    g_colours.emplace_back(zeroone_rn(g));
    g_colours.emplace_back(zeroone_rn(g));
    g_colours.emplace_back(zeroone_rn(g));
    g_colours.emplace_back(1.0f);
  }
  auto stop = ch::high_resolution_clock::now();
  cout << ch::duration_cast<ch::milliseconds>(stop - start).count() << "ms\n";
  
  Test_Eigen();

  struct Test {
    char a;
    float b[4];
  };

  Test t;

  printf("&Test     : %p\n", &t);
  printf("&Test.a   : %p\n", &t.a);
  printf("&Test.b   : %p\n", &t.b);
  printf("&Test.b[0]: %p\n", &t.b[0]);
  printf("Test.b    : %p\n", t.b);
  printf("sizeof    : %llu\n", sizeof(Test));

  t.a = 'a';
  t.b[0] = 0.0f;

  std::ofstream test_file("C:/temp/test.bin", std::ios::binary);
  test_file.write(&t.a, sizeof(t));
  test_file.close();

  
  return 0;
}

