#include "ply.h"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <regex>
#include <stdexcept>

using std::cout;
using std::string;

auto const g_case_insensitive = std::regex::ECMAScript | std::regex::icase;

PLYMesh::PLYMesh(string filename)
    : is_ply_(false),
      format_(kUnknown) {
  std::ifstream ply_file(filename);

  ReadHeader(ply_file);
  ReadVertices(ply_file);
}

void PLYMesh::ReadHeader(std::istream & stream) {
  string line;

  is_ply_ = false;
  std::regex ply_regex("ply", g_case_insensitive);
  std::regex end_header_regex("end_header", g_case_insensitive);

  // Look for ply header
  while (std::getline(stream, line)) {
    if (std::regex_match(line, ply_regex)) {
      is_ply_ = true;
      break;
    }
    if (std::regex_match(line, end_header_regex))
      break;
  }

  if (!is_ply_)
    throw std::runtime_error("PLYMesh: Is not a PLY file");

  format_ = kUnknown;
  std::regex format_regex("format\\s+(\\w+)\\s+([0-9]+\\.?[0-9]*)",
                          g_case_insensitive);

  while (std::getline(stream, line)) {
    std::smatch matches;
    if (std::regex_match(line, matches, format_regex)) {
      string format_str = matches[1];
      std::transform(begin(format_str), end(format_str), begin(format_str), ::tolower);
      if (format_str == "ascii")
        format_ = kASCII;
      format_version_ = matches[2];
      break;
    }

    if (std::regex_match(line, end_header_regex))
      break;
  }

  if (format_ == kUnknown)
    throw std::runtime_error("PLYMesh: Unknown format");

  std::regex comment_regex("comment\\s+(.*)\\s*$", g_case_insensitive);
  std::regex element_regex("element\\s+(\\w+)\\s+(\\d+)", g_case_insensitive);

  while (std::getline(stream, line)) {
    std::smatch matches;
    if (std::regex_match(line, matches, comment_regex)) {
      cout << "Comment: " << matches[1] << '\n';
    } else if (std::regex_match(line, matches, element_regex)) {
      cout << "Element: " << matches[1] << '\n';
      Element element;
      element.name = matches[1];
      element.count = std::stoull(matches[2]);

      ReadElement(stream, element);

      elements_.push_back(element);
    } else if (std::regex_match(line, matches, end_header_regex)) {
      break;
    }
  }
}

void PLYMesh::ReadElement(std::istream & stream, Element & element) {
  std::regex property_regex("property\\s+(\\w+)\\s+(\\w+)\\s*$", g_case_insensitive);
  std::regex property_list_regex("property\\s+list\\s+(\\w+)\\s+(\\w+)\\s+(\\w+)\\s*$", g_case_insensitive);

  string line;
  size_t pos = stream.tellg();

  while (std::getline(stream, line)) {
    std::smatch matches;

    if (std::regex_match(line, matches, property_regex)) {
      cout << "Property[type=" << matches[1] << ",name=" << matches[2] << "]\n";
      element.properties.push_back(matches[2]);
    } else if (std::regex_match(line, matches, property_list_regex)) {
      cout << "Property[type=list,subtype=" << matches[1] << ' ' << matches[2] << ",name=" << matches[3] << "]\n";
      element.properties.push_back(matches[3]);
    } else {
      stream.seekg(pos);
      break;
    }

    pos = stream.tellg();
  }
}

void PLYMesh::ReadVertices(std::istream & stream) {
  size_t vertex_count = 0;
  for (auto const & element : elements_) {
    if (element.name == "vertex")
      vertex_count = element.count;
  }

  // TODO this doesn't match scientific notation
  std::regex vertex_regex("^\\s*(\\-?[0-9]+\\.?[0-9]*)\\s+(\\-?[0-9]+\\.?[0-9]*)\\s+"
                          "(\\-?[0-9]+\\.?[0-9]*)\\s+(\\-?[0-9]+\\.?[0-9]*)\\s+"
                          "(\\-?[0-9]+\\.?[0-9]*)\\s*$");
  string line;
  size_t i = 0;
  while (std::getline(stream, line) && i < vertex_count) {
    std::smatch matches;
    if (std::regex_match(line, matches, vertex_regex)) {
      vertices_.emplace_back(
        Vertex{
          std::stof(matches[1]), std::stof(matches[2]), std::stof(matches[3]),
          std::stof(matches[4]), std::stof(matches[5])
        });
    } else {
      break;
    }
    ++i;
  }

  cout << "vert\n";
}

void PLYMesh::ReadFaces(std::istream & stream) {

}
